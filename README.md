# adv-helper

## Add a new project

adv add project puma [E4567282436] [BE Entwicklung]

puma = Name of the new project
E4567282436 = PSP element (optional)
BE Entwicklung = Default todo (optional)

## Start timeframe for one project with a description

adv start puma [some stuff]

puma = Name of the project
some stuff = Description for entry (optional)

## Stop timeframe

adv stop

## Cancel timeframe

adv cancel

## Create ADV import file

adv create import [year|year-month|year-month-day|]

 = Creates import file with all timeframe from current month
year = Creates import file with all timeframe from given year (example: 2018)
year-month = Creates import file with all timeframe from given year and month (example: 2018-09)
year-month-day = Creates import file with all timeframe from given year, month an day (example: 2018-09-01)

## Edit project psp value

adv edit project psp puma E4567282436

Set or change the PSP element of project puma to E4567282436

## Edit project default todo

adv edit project todo puma BE Entwicklung

## Create report about logged timeframes for given period of time

adv report puma [year|year-month|year-month-day|]

 = Report all logged timeframes of project puma
year = Report all logged timeframes of project puma for given year
year-month = Report all loagged timeframes of project puma for given year and month
year-month-day = Report all loagged timeframes of project puma for given year, month and day
