package de.yellowshoes.advhelper.project;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

import org.apache.commons.lang3.StringUtils;

import de.yellowshoes.advhelper.booking.BookingContainer;

/**
 * Container for storing information about one project.
 * 
 * @author calibanatspace
 *
 */
@XmlRootElement(name = "project")
@XmlType(propOrder = { "name", "description", "psp", "defaultActivity", "bookings"})
public class ProjectContainer {

	/** Name of the project, should be short name */
	private String name;
	/** Description of the project, if needed */
	private String description;
	/** PSP element used for this element */
	private String psp;
	/** Default activity, that is used in the booking if nothing else is used */
	private String defaultActivity;
	/** {@link List} of {@link BookingContainer} for this project */
	private List<BookingContainer> bookings;

	public String getName() {
		return name;
	}

	@XmlElement(name = "name")
	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	@XmlElement(name = "description")
	public void setDescription(String description) {
		this.description = description;
	}

	public String getDefaultActivity() {
		return defaultActivity;
	}

	@XmlElement(name = "defaultactivity")
	public void setDefaultActivity(String defaultActivity) {
		this.defaultActivity = defaultActivity;
	}

	public List<BookingContainer> getBookings() {
		if( bookings == null ) {
			bookings = new ArrayList<>();
		}
		return bookings;
	}

	@XmlElementWrapper(name = "bookings")
	@XmlElement(name = "booking")
	public void setBookings(List<BookingContainer> bookings) {
		this.bookings = bookings;
	}

	public String getPsp() {
		return psp;
	}

	@XmlElement(name = "psp")
	public void setPsp(String psp) {
		this.psp = psp;
	}

	@Override
	public String toString() {
		return "Name: " + getName() + ", PSP: " + getPsp() + ", DefaultActivity: " + StringUtils.defaultString(getDefaultActivity());
	}
}
