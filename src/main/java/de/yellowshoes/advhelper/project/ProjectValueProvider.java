package de.yellowshoes.advhelper.project;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.MethodParameter;
import org.springframework.shell.CompletionContext;
import org.springframework.shell.CompletionProposal;
import org.springframework.shell.standard.ValueProviderSupport;
import de.yellowshoes.advhelper.storage.StorageService;
import org.springframework.stereotype.Component;

/**
 * Provider to return all project names
 * 
 * @author calibanatspace
 *
 */
@Component
public class ProjectValueProvider extends ValueProviderSupport {

	@Autowired
	private StorageService storageService;
	
	@Override
	public List<CompletionProposal> complete(MethodParameter parameter, CompletionContext completionContext,
			String[] hints) {
		List<ProjectContainer> projects = storageService.getAllProjects();
		return projects.stream().map( p -> p.getName() ).map( CompletionProposal::new).collect(Collectors.toList());
	}

}
