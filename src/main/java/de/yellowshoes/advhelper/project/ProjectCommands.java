package de.yellowshoes.advhelper.project;

import java.util.List;
import java.util.Optional;

import javax.validation.constraints.NotNull;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.shell.standard.ShellComponent;
import org.springframework.shell.standard.ShellMethod;
import org.springframework.shell.standard.ShellOption;

import de.yellowshoes.advhelper.storage.StorageService;

@ShellComponent(value = "All commands to handle projects")
public class ProjectCommands {

	@Autowired
	private StorageService storageService;
	
	@ShellMethod(value = "Add a new project")
	public String addProject( @NotNull String name, @NotNull String psp, String defaultActivity ) {
		ProjectContainer newProject = new ProjectContainer();
		newProject.setName(name);
		newProject.setPsp(psp);
		newProject.setDefaultActivity(defaultActivity);
		
		ProjectContainer addedProject = storageService.addNewProject( newProject );
		
		return "New project added: " + addedProject.toString();
	}
	
	@ShellMethod(value = "Edit an existing project")
	public String editProject( @NotNull @ShellOption(valueProvider = ProjectValueProvider.class) String name, String psp, String defaultActivity ) {
		Optional<ProjectContainer> project = storageService.getProject( name );
		
		if( project.isPresent() ) {
			ProjectContainer p = project.get();
			p.setPsp(psp);
			p.setDefaultActivity(defaultActivity);
			return "Project updated: " + p.toString();
		} else {
			return "Project with name " + name + " not found!";
		}
	}
	
	@ShellMethod(value = "List all active projects")
	public String listProjects() {
		List<ProjectContainer> allProjects = storageService.getAllProjects();
		
		StringBuilder builder = new StringBuilder();
		
		if( allProjects.isEmpty() ) {
			builder.append( "No projects defined!");
		} else {
			allProjects.forEach( project -> {
				builder.append( project.toString() ).append( "\n" );
			});
		}
		return builder.toString();
	}
	
}
