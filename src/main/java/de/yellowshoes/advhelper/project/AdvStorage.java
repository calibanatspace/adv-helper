package de.yellowshoes.advhelper.project;

import java.util.List;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class AdvStorage {

	private List<ProjectContainer> projects = null;

	public List<ProjectContainer> getProjects() {
		return projects;
	}

	@XmlElementWrapper( name = "projects")
	@XmlElement( name = "project" )
	public void setProjects(List<ProjectContainer> projects) {
		this.projects = projects;
	}
	
}
