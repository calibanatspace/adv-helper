package de.yellowshoes.advhelper.storage;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;

import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;

import de.yellowshoes.advhelper.booking.BookingContainer;
import de.yellowshoes.advhelper.project.AdvStorage;
import de.yellowshoes.advhelper.project.ProjectContainer;

@Service
public class StorageService {

	String currentUserHome = System.getProperty("user.home");
	String advHelperHome = currentUserHome + File.separator + "advhelper";
	String advCurrentFilePath = advHelperHome + File.separator + "adv-storage.xml";

	List<ProjectContainer> projects = null;

	public StorageService() {
		loadProjects();
	}

	/**
	 * Return all defined projects.
	 * 
	 * @return
	 */
	public List<ProjectContainer> getAllProjects() {
		// Projects not initalized/loaded
		if (projects == null) {
			loadProjects();
		}

		if (projects != null) {
			return new ArrayList<>(projects);
		} else {
			return new ArrayList<>();
		}
	}

	private void loadProjects() {
		if (isStorageFileExists() == false) {
			initStorageFile();
		}

		try {
			JAXBContext context = setupJAXBContext();
			Unmarshaller unmarshaller = context.createUnmarshaller();
			AdvStorage storage = (AdvStorage) unmarshaller.unmarshal(new File(advCurrentFilePath));
			if (storage != null) {
				projects = storage.getProjects();
			}
		} catch (JAXBException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	private void initStorageFile() {
		System.out.println("Init storage file");

		File folder = new File(advHelperHome);
		if (folder.exists() == false) {
			if (folder.mkdir() == false) {
				System.err.println("Folder " + advHelperHome + " could not be created");
				throw new RuntimeException("Could not create folder " + advHelperHome);
			}
		}
		AdvStorage storage = new AdvStorage();
		storage.setProjects(new ArrayList<>());

		saveProjects(storage, advCurrentFilePath);
	}

	private void saveProjects(AdvStorage storage, String filePath) {
		try {
			JAXBContext context = setupJAXBContext();
			Marshaller marshaller = context.createMarshaller();
			marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
			marshaller.marshal(storage, new File(filePath));
		} catch (JAXBException e) {
			System.err.println("File " + filePath + " could not be stored");
			e.printStackTrace();
		}
	}

	private JAXBContext setupJAXBContext() throws JAXBException {
		JAXBContext context = JAXBContext.newInstance(ProjectContainer.class, BookingContainer.class, ArrayList.class,
				AdvStorage.class);
		return context;
	}

	private boolean isStorageFileExists() {
		return new File(advCurrentFilePath).exists();
	}

	/**
	 * Save the current configuration.
	 */
	public void save() {
		AdvStorage storage = new AdvStorage();
		storage.setProjects(projects);
		saveProjects(storage, advCurrentFilePath);
		System.out.println("Data saved!");
	}

	/**
	 * Add a new project to the data.
	 * 
	 * @param newProject
	 * @return
	 */
	public ProjectContainer addNewProject(ProjectContainer newProject) {
		if (projects != null) {
			projects.add(newProject);
			return newProject;
		}
		return null;
	}

	/**
	 * Get existing project by name.
	 * 
	 * @param name
	 * @return
	 */
	public Optional<ProjectContainer> getProject(String name) {
		return getAllProjects().parallelStream().filter(p -> StringUtils.equals(name, p.getName())).findFirst();
	}

}
