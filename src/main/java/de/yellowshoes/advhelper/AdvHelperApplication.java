package de.yellowshoes.advhelper;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class AdvHelperApplication {

	public static void main(String[] args) {
		SpringApplication.run(AdvHelperApplication.class, args);
	}

}
