package de.yellowshoes.advhelper.shell;

import org.jline.utils.AttributedString;
import org.jline.utils.AttributedStyle;
import org.springframework.shell.jline.PromptProvider;
import org.springframework.stereotype.Component;

@Component
public class AdvPromptProvider implements PromptProvider {

	@Override
	public AttributedString getPrompt() {
		return new AttributedString("advhelper:>", AttributedStyle.DEFAULT.foreground(AttributedStyle.GREEN));
	}

}
