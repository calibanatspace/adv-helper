package de.yellowshoes.advhelper.shell;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.shell.standard.ShellComponent;
import org.springframework.shell.standard.ShellMethod;
import org.springframework.shell.standard.commands.Quit;

import de.yellowshoes.advhelper.storage.StorageService;

@ShellComponent
public class QuitCommand implements Quit.Command {

	@Autowired
	private StorageService storageService;
	
	@ShellMethod(value = "Quit the helper", key = { "quit", "exit" })
	public void quit() {
		storageService.save();
		System.exit(0);
	}
}
