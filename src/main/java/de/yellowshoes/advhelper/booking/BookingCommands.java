package de.yellowshoes.advhelper.booking;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.shell.standard.ShellComponent;
import org.springframework.shell.standard.ShellMethod;
import org.springframework.shell.standard.ShellOption;

import de.yellowshoes.advhelper.project.ProjectContainer;
import de.yellowshoes.advhelper.project.ProjectValueProvider;
import de.yellowshoes.advhelper.storage.StorageService;

@ShellComponent(value = "All commands to handle bookings")
public class BookingCommands {

	@Autowired
	private StorageService storageService;

	@ShellMethod(value = "Start a new booking now")
	public String startBookingNow(@ShellOption(valueProvider = ProjectValueProvider.class) String projectName,
			String activity) {
		Optional<ProjectContainer> project = storageService.getProject(projectName);
		if (project.isPresent()) {
			ProjectContainer p = project.get();
			BookingContainer booking = new BookingContainer();
			booking.setActivity(activity);
			booking.setStartTime(LocalDateTime.now());
			p.getBookings().add(booking);
			return "New booking for project " + projectName + " started: " + booking.toString();
		} else {
			return "Project with name " + projectName + " not found. No booking started!";
		}
	}

	@ShellMethod(value = "Start a new booking with starttime")
	public String startBooking(@ShellOption(valueProvider = ProjectValueProvider.class) String projectName,
			String activity, LocalDateTime startTime) {
		Optional<ProjectContainer> project = storageService.getProject(projectName);
		if (project.isPresent()) {
			ProjectContainer p = project.get();
			BookingContainer booking = new BookingContainer();
			booking.setActivity(activity);
			booking.setStartTime(startTime);
			p.getBookings().add(booking);
			return "New booking for project " + projectName + " started: " + booking.toString();
		} else {
			return "Project with name " + projectName + " not found. No booking started!";
		}
	}

	@ShellMethod(value = "End a booking")
	public String endBookingNow(@ShellOption(valueProvider = ProjectValueProvider.class) String projectName) {
		Optional<ProjectContainer> project = storageService.getProject(projectName);
		if( project.isPresent() ) {
			ProjectContainer p = project.get();
			
			if( p.getBookings() == null || p.getBookings().isEmpty() ) {
				return "Project with name " + projectName + " has no bookings!";
			}

			List<BookingContainer> bookings = p.getBookings();
			Optional<BookingContainer> booking = bookings.parallelStream().filter( b -> b.getEndTime() == null ).findFirst();
			
			if( booking.isPresent() ) {
				BookingContainer b = booking.get();
				b.setEndTime(LocalDateTime.now());
				return "Project with name " + projectName + ", set endtime: " + b.toString();
			} else {
				return "Project with name " + projectName + " has no booking without endtime!";
			}
		} else {
			return "Project with name " + projectName + " not found!";
		}
	}

	@ShellMethod(value = "Add a new booking with start and endtime")
	public String addBooking(String project, String activity, LocalDateTime startTime, LocalDateTime endTime) {
		return "";
	}

	@ShellMethod(value = "List booking for a project")
	public String listBookingsForProject(@ShellOption(valueProvider = ProjectValueProvider.class) String projectName) {
		Optional<ProjectContainer> project = storageService.getProject(projectName);
		
		if( project.isPresent() ) {
			ProjectContainer p = project.get();
			
			if( p.getBookings() == null || p.getBookings().isEmpty() ) {
				return "Project with name " + projectName + " has no bookings!";
			}
			
			List<BookingContainer> bookings = p.getBookings();
			StringBuilder builder = new StringBuilder();
			bookings.forEach( b -> builder.append( b.toString() ).append( "\n" ));
			return builder.toString();
		} else {
			return "Project with name " + projectName + " not found!";
		}
	}
//	public String delete( String project, )

//	public String edit()
}
