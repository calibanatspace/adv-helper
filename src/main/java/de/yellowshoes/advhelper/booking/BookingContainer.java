package de.yellowshoes.advhelper.booking;

import java.time.LocalDateTime;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;

import de.yellowshoes.advhelper.jaxb.LocalDateTimeAdapter;

/**
 * Container for storing the data for a booking entry.
 * 
 * @author calibanatspace
 *
 */
@XmlRootElement(name = "booking")
@XmlType(propOrder = { "activity", "startTime", "endTime"})
public class BookingContainer {

	/** Activity in the project or defined description for adv booking. */
	private String activity;
	/** Timestamp when this booking started. */
	private LocalDateTime startTime;
	/** Timestamp when this booking ended. */
	private LocalDateTime endTime;

	public String getActivity() {
		return activity;
	}

	@XmlElement(name = "activity")
	public void setActivity(String activity) {
		this.activity = activity;
	}

	public LocalDateTime getStartTime() {
		return startTime;
	}

	@XmlElement(name = "starttime")
	@XmlJavaTypeAdapter( value = LocalDateTimeAdapter.class)
	public void setStartTime(LocalDateTime startTime) {
		this.startTime = startTime;
	}

	public LocalDateTime getEndTime() {
		return endTime;
	}

	@XmlElement(name = "endtime")
	@XmlJavaTypeAdapter( value = LocalDateTimeAdapter.class)
	public void setEndTime(LocalDateTime endTime) {
		this.endTime = endTime;
	}

	@Override
	public String toString() {
		return "Activity: " + activity + ", Starttime: " + startTime + ", Endtime: " + endTime;
	}
}
